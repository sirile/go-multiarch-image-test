# Go multiarch example with GitLab CI/CD

This example project returns a yin and yang-image with colors based on the hostname. It can be used to demonstrate scaling in various runtime environments, for example in a Kubernetes cluster. It demonstrates minimal container size by using a statically linked executable in a scratch-container.

GitLab CI/CD-pipelines are used to run unit and coverage tests as well as to build multiarchitecture image for AMD64 and ARM64.

## Commands

### Run locally

```bash
go run image.go
```

### Build locally

```bash
go build
```

### Running tests

```bash
go test
```

### Creating and checking coverage report

```bash
go test -coverprofile=coverage.out
go tool cover -html=coverage.out
```

### Local multiarchitecture Docker build and push

```bash
docker buildx build --platform linux/amd64,linux/arm64 -t sirile/go-multiarch-image-test --output type=registry .
```

### Running from DockerHub

```bash
docker run --rm -p 8080:80 sirile/go-multiarch-image-test 
```

### Running from GitLab registry

```bash
docker run --rm -p 8080:80 registry.gitlab.com/sirile/go-multiarch-image-test
```

### Accessing the local image

Point the browser to <http://localhost:8080>.
