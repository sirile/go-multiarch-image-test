FROM --platform=$BUILDPLATFORM golang:1.19-alpine as build
ARG TARGETOS
ARG TARGETARCH
WORKDIR /app
COPY image.go ./
COPY go.mod ./
RUN go mod download

RUN CGO_ENABLED=0 GOOS=${TARGETOS} GOARCH=${TARGETARCH} \
    go build -ldflags="-s -w" -o image-test .

FROM scratch
COPY --from=build app/image-test /
EXPOSE 80
ENTRYPOINT [ "./image-test" ]

# docker buildx build --platform linux/amd64,linux/arm64 -t sirile/go-test --output type=registry .