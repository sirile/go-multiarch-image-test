package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestImage(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(handler)

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check that the response body contains the expected title.
	if !strings.Contains(rr.Body.String(), `<title>Go scaling demo, go!</title>`) {
		t.Errorf("handler returned unexpected body: got %v want body containing %v",
			rr.Body.String(), `<title>Go scaling demo, go!</title>`)
	}

}
